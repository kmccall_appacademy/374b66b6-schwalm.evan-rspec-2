def reverser
  words = yield.split(' ')
  reversed = []

  words.each do |word|
    reversed.push(word.reverse)
  end

  reversed.join(' ')
end

def adder(number = 1)
  yield + number
end

def repeater(number = 1)
  number.times do
    yield
  end
end

def measure(repeat = 1)
  start = Time.now
  repeat.times { |x| yield x }
  finish = Time.now

  (finish - start) / repeat
end
